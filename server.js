//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

var bodyparser =require('body-parser');
app.use(bodyparser.json())
app.use(bodyparser.urlencoded({ extended: true }));

const MongoClient = require("mongodb").MongoClient;
const ObjectId = require("mongodb").ObjectID;
// liga del cluster Mongo Atlas
const CONNECTION_URL = "mongodb+srv://m001-student:pollito4@carlos-m001-z2fzn.mongodb.net/test?retryWrites=true&w=majority"
const DATABASE_NAME = "Practitioner";

var database, collection;

app.listen(3000, () => {
    MongoClient.connect(CONNECTION_URL, { useNewUrlParser: true }, (error, client) => {
        if(error) {
            throw error;
        }
        database = client.db(DATABASE_NAME);
        collection = database.collection("Movimientos");
        console.log("Connected to `" + DATABASE_NAME + "`!");
    });
});



console.log('todo list RESTful API server started on: ' + port);

var MovimientosJSON = require('./movimientos_V2.json');

app.post("/Movimientos/Agregar",function (req,res) {
  console.log(req.body);
  collection.insertOne(req.body, function(err, res) {
    if (err) throw err;
    console.log("1 document inserted");
  });
});

app.get("/Movimientos/Visualizar",function (req,res) {
  collection.find({}).toArray(function (err,result) {
        if(err) throw err;
        console.log(result);
        res.send(result);     
  });
});

app.get("/Movimientos/Visualizar/:cliente",function (req,res) {
  var query={nombre:"Carlos Estrada"};
  console.log(query)
  collection.find(query).toArray(function (err,result) {
        if(err) throw err;
        console.log(result);
        res.send(result);     
  });
});